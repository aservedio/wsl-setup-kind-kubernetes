#make

# hide entering/leaving directory messages
MAKEFLAGS += --no-print-directory

DIRS = $(shell find . -maxdepth 1 -type d -not -regex '^\.$$' -not -regex '^\./\..*' | cut -d'/' -f2- | sort -h)
DIRS_REVERSE = $(shell find . -maxdepth 1 -type d -not -regex '^\.$$' -not -regex '^\./\..*' | cut -d'/' -f2- | sort -hr)

all: start

.PHONY: start
start:
	@echo "# start setup"
	@$(foreach DIR,$(DIRS),$(MAKE) -C $(DIR);)

.PHONY: stop
stop:
	@echo "# stop setup"
	@$(foreach DIR,$(DIRS_REVERSE),$(MAKE) -C $(DIR) stop;)
