# WSL - Setup KinD Kubernetes

This simple project installs a local Kubernetes cluster using KinD,
ensures basic K8 tooling and uses kubefwd to connect all K8 services
to the local environment.

## Requirements

- a local docker engine

## How to run

- clone project into your WSL distro
- run `make`
